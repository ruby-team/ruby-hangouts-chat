#########################################################
# This file has been automatically generated by gem2tgz #
#########################################################
# -*- encoding: utf-8 -*-
# stub: hangouts-chat 0.0.5 ruby lib

Gem::Specification.new do |s|
  s.name = "hangouts-chat".freeze
  s.version = "0.0.5"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["enzinia".freeze]
  s.date = "2018-03-31"
  s.description = "Send messages to G Suite Hangouts Chat rooms using incoming webhooks and Net::HTTP::Post".freeze
  s.email = "vkukovskij@gmail.com".freeze
  s.files = ["CHANGELOG.md".freeze, "LICENSE".freeze, "README.md".freeze, "Rakefile".freeze, "lib/hangouts_chat.rb".freeze, "lib/hangouts_chat/exceptions.rb".freeze, "lib/hangouts_chat/http.rb".freeze, "lib/hangouts_chat/version.rb".freeze, "test/hangouts_chat/http_test.rb".freeze, "test/hangouts_chat_test.rb".freeze, "test/test_helper.rb".freeze]
  s.homepage = "https://github.com/enzinia/hangouts-chat".freeze
  s.licenses = ["MIT".freeze]
  s.rubygems_version = "2.7.6".freeze
  s.summary = "Library for sending messages to Hangouts Chat rooms".freeze
  s.test_files = ["test/hangouts_chat/http_test.rb".freeze, "test/hangouts_chat_test.rb".freeze, "test/test_helper.rb".freeze]

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_development_dependency(%q<bundler>.freeze, ["~> 1"])
      s.add_development_dependency(%q<minitest>.freeze, ["~> 5"])
      s.add_development_dependency(%q<rake>.freeze, ["~> 12"])
      s.add_development_dependency(%q<rubocop>.freeze, ["<= 0.54.0"])
      s.add_development_dependency(%q<webmock>.freeze, ["~> 3"])
      s.add_development_dependency(%q<yard>.freeze, ["> 0.9.11", "~> 0.9"])
    else
      s.add_dependency(%q<bundler>.freeze, ["~> 1"])
      s.add_dependency(%q<minitest>.freeze, ["~> 5"])
      s.add_dependency(%q<rake>.freeze, ["~> 12"])
      s.add_dependency(%q<rubocop>.freeze, ["<= 0.54.0"])
      s.add_dependency(%q<webmock>.freeze, ["~> 3"])
      s.add_dependency(%q<yard>.freeze, ["> 0.9.11", "~> 0.9"])
    end
  else
    s.add_dependency(%q<bundler>.freeze, ["~> 1"])
    s.add_dependency(%q<minitest>.freeze, ["~> 5"])
    s.add_dependency(%q<rake>.freeze, ["~> 12"])
    s.add_dependency(%q<rubocop>.freeze, ["<= 0.54.0"])
    s.add_dependency(%q<webmock>.freeze, ["~> 3"])
    s.add_dependency(%q<yard>.freeze, ["> 0.9.11", "~> 0.9"])
  end
end
